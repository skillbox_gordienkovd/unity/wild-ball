public static class GlobalStringVars
{
    #region Input vars

    public const string HorizontalAxis = "Horizontal";
    public const string VerticalAxis = "Vertical";
    public const string JumpButton = "Jump";

    #endregion
}