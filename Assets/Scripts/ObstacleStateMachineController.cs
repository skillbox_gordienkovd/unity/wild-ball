using System.Collections;
using UnityEngine;


[RequireComponent(typeof(Animator))]
public class ObstacleStateMachineController : MonoBehaviour
{
    private AnimationClip[] _clips;
    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();

        _clips = _animator.runtimeAnimatorController.animationClips;
    }

    private void Start()
    {
        StartCoroutine(PlayRandomClip());
    }

    private IEnumerator PlayRandomClip()
    {
        while (true)
        {
            var randomClipIndex = Random.Range(0, _clips.Length);
            var randomClip = _clips[randomClipIndex];

            _animator.Play(randomClip.name);

            yield return new WaitForSeconds(randomClip.length);
        }
    }
}