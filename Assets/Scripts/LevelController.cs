using Player;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    public void LoadScene(int sceneIndex)
    {
        SceneManager.LoadScene(sceneBuildIndex: sceneIndex);
    }

    private static void ReloadScene()
    {
        SceneManager.LoadScene(sceneBuildIndex: SceneManager.GetActiveScene().buildIndex);
    }

    private static void LoadNextScene()
    {
        SceneManager.LoadScene(sceneBuildIndex: SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void OnEnable()
    {
        PlayerDeath.PlayerDead += ReloadScene;
        NextLevelPointController.LevelCompleted += LoadNextScene;
    }

    private void OnDisable()
    {
        PlayerDeath.PlayerDead -= ReloadScene;
        NextLevelPointController.LevelCompleted -= LoadNextScene;
    }
}