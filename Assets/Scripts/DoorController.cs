using UnityEngine;

public class DoorController : MonoBehaviour
{
    [SerializeField] private Transform playerTransform;
    [SerializeField] private GameObject doorTooltip;

    private bool _playerNearby;

    private void Update()
    {
        if (!_playerNearby || !Input.GetKeyDown(KeyCode.E)) return;
        
        doorTooltip.SetActive(false);
        Destroy(gameObject);
    }

    private void FixedUpdate()
    {
        var distance = Vector3.Distance(transform.position, playerTransform.position);

        _playerNearby = distance <= 0.8f;

        ShowTooltip();
    }

    private void ShowTooltip()
    {
        doorTooltip.SetActive(_playerNearby);
    }
}
