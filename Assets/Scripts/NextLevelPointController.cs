using System;
using UnityEngine;

public class NextLevelPointController : MonoBehaviour
{
    public static event Action LevelCompleted;

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            LevelCompleted?.Invoke();
        }
    }
}