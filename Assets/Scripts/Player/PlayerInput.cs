using UnityEngine;

namespace Player
{
    public class PlayerInput : MonoBehaviour
    {
        public Vector3 Movement { get; private set; }
        
        private void Update()
        {
            var horizontal = Input.GetAxis(GlobalStringVars.HorizontalAxis);
            var vertical = Input.GetAxis(GlobalStringVars.VerticalAxis);
        
            Movement = new Vector3(horizontal, 0, vertical).normalized;
        }
    }
}