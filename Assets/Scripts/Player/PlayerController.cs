﻿using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(PlayerMovement))]
    [RequireComponent(typeof(PlayerInput))]
    [RequireComponent(typeof(PlayerCameraMovement))]
    [RequireComponent(typeof(PlayerDeath))]
    public class PlayerController : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField, Range(0, 10)] private float movementSpeed = 2.0f;
        
        private PlayerMovement _playerMovement;
        private PlayerInput _playerInput;
        private PlayerCameraMovement _playerCameraMovement;

        private void Awake()
        {
            _playerMovement = GetComponent<PlayerMovement>();
            _playerInput = GetComponent<PlayerInput>();
            _playerCameraMovement = GetComponent<PlayerCameraMovement>();
        }

        private void Start()
        {
            _playerCameraMovement.SetOffset(transform.position);
        }

        private void FixedUpdate()
        {
            Move();
            MovePlayerCamera();
        }

        private void Move()
        {
            _playerMovement.MoveCharacter(movement: _playerInput.Movement, speed: movementSpeed);
        }

        private void MovePlayerCamera()
        {
            _playerCameraMovement.MovePlayerCamera(transform.position);
        }
    }
}