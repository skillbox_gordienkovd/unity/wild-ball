﻿using System;
using UnityEngine;

namespace Player
{
    public class PlayerDeath : MonoBehaviour
    {
        public static event Action PlayerDead;

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.tag.Equals("Obstacle"))
            {
                PlayerDead?.Invoke();
            }
        }
    }
}