using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(Transform))]
    public class PlayerCameraMovement : MonoBehaviour
    {
        [SerializeField] private Transform cameraTransform;
        private Vector3 _offset;

        public void SetOffset(Vector3 playerTransformPosition)
        {
            _offset = cameraTransform.position - playerTransformPosition;
        }

        public void MovePlayerCamera(Vector3 playerTransformPosition)
        {
            cameraTransform.position = playerTransformPosition + _offset;
        }
    }
}
