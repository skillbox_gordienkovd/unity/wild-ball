using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerMovement : MonoBehaviour
    {
        private Rigidbody _playerRigidBody;

        private void Awake()
        {
            _playerRigidBody = GetComponent<Rigidbody>();
        }

        public void MoveCharacter(Vector3 movement, float speed)
        {
            _playerRigidBody.AddForce(movement * speed);
        }
    }
}